<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>List Student</title>
</head>
<style>
    .container {
        display: flex;
        flex-direction: column;
        height: auto;
        width: 34vw;
        margin: 2rem 33vw;
        border: 2px solid #385e8b;
        padding: 1rem;
    }

    .form-group {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: space-around;
    }

    .form-child {
        display: flex;
        justify-content: space-around;
        align-items: center;
        margin-bottom: 1rem;
    }

    .form-text {
        width: 60px;
    }

    .sub-form-text {
        display: flex;
        width: 180px;
        height: 30px;
    }

    .search-child-input {
        display: flex;
        justify-content: space-around;
        justify-content: center;
        width: 200px;
    }

    .input {
        height: inherit;
        width: inherit;
        border: 2px solid #9cafc6;
    }

    .search {
        display: flex;
        justify-content: center;
    }

    .sub-search {
        color: #cfdded;
        background-color: #4f81bd;
        border-radius: 5px;
        border: 2px solid #385e8b;
        height: 30px;
        width: 80px
    }

    .amount {
        display: flex;
        justify-content: flex-start;
    }

    .add {
        display: flex;
        justify-content: flex-end;
    }

    .sub-add {
        color: #cfdded;
        background-color: #4f81bd;
        border-radius: 5px;
        border: 2px solid #385e8b;
        height: 30px;
        width: 80px
    }

    .remove {
        margin-right: 10px;
        height: 30px;
        background-color: #4f81bd;
    }

    .edit {
        height: 30px;
        background-color: #4f81bd;
    }
</style>

<body>
    <?php
    include 'Connect.php';
    if (isset($_GET['searchKhoa']) && !empty($_GET['searchKhoa'])) {
        $key = $_GET['searchKhoa'];
        $sql = "SELECT id, name, gender, faculty FROM student WHERE faculty LIKE '%$key%'";
    } else if (isset($_GET['searchTuKhoa']) && !empty($_GET['searchTuKhoa'])) {
        $key = $_GET['searchTuKhoa'];
        $sql = "SELECT id, name, gender, faculty FROM student WHERE name LIKE '%$key%' OR address LIKE '%$key%'";
    } else {
        $sql = "SELECT id, name, gender, faculty FROM student ";
    }
    $result = mysqli_query($conn, $sql);
    ?>
    <div class="container">
        <h3 align="center">Danh sách sinh viên</h3>
        <table class="search-form">
            <tr>
                <td>
                    <form action="" method="get">
                        <div class="form-group">
                            <div class="form-child">
                                <label class="form-text">
                                    Khoa
                                </label>
                                <div class="sub-form-text">
                                    <input type="text" name="searchKhoa" placeholder="Nhập tên khoa" value="<?php if (isset($_GET['searchKhoa']) && !empty($_GET['searchKhoa'])) {
                                                                                                                echo $_GET['searchKhoa'];
                                                                                                            } ?>">
                                </div>
                            </div>
                            <div class="form-child">
                                <label class="form-text">
                                    Từ khóa
                                </label>
                                <div class="sub-form-text">
                                    <input type="text" name="searchTuKhoa" placeholder="Nhập từ khóa cần tìm" value="<?php if (isset($_GET['searchTuKhoa']) && !empty($_GET['searchTuKhoa'])) {
                                                                                                                            echo $_GET['searchTuKhoa'];
                                                                                                                        } ?>">
                                </div>
                            </div>
                        </div>
                        <div class="search">
                            <input type="submit" class="sub-search" name="search" value="Tìm kiếm" />
                        </div>
                        <div></br></div>
                        <div class="add">
                            <label class="sub-add">
                                Thêm mới
                            </label>
                        </div>
                    </form>

                </td>
            </tr>
        </table>
        <table style="width: 550px;">
            <tr>
                <th>No</th>
                <th>Họ và tên</th>
                <th>Giới tính</th>
                <th>Khoa</th>
                <th colspan="2">Action</th>
            </tr>
            <?php
            while ($row = mysqli_fetch_assoc($result)) {
                $maSv = $row['id'];
                $tenSv = $row['name'];
                $gioitinh = $row['gender'];
                $khoa = $row['faculty'];
            ?>
                <tr>
                    <td><?php echo $maSv ?></td>
                    <td><?php echo $tenSv ?></td>
                    <td>
                        <?php if ($gioitinh == '1') echo "Nam";
                        else echo "Nữ" ?>
                    </td>
                    <td><?php if ($khoa == 'MAT') echo "Khoa học máy tính";
                        else echo "Khoa học dữ liệu" ?></td>
                    <td> <a href="suadulieu.php?id=<?php echo $maSv; ?>" class="edit">Sửa</a></td>
                    <td><a href="xulyxoa.php?id=<?php echo $maSv; ?>" class="remove">Xóa</a></td>
                </tr>
            <?php
            }
            ?>
        </table>
    </div>
</body>

</html>